﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using System;

public partial class Crystal : MonoBehaviour, IPoolable
{
    [SerializeField] protected SpriteRenderer spriteRenderer;

    public CrystalType Type { get; private set; }
    public bool IsAnimationRunning { get; private set; }

    private Cell currentCell;

    #region IPoolable
    [SerializeField] protected string poolableName;
    public string Name => poolableName;
    #endregion

    public void Initialize(CrystalType type, Cell cell)
    {
        Type = type;
        BindToCell(cell);
        spriteRenderer.sprite = GameManager.Settings.GetCrystalSprite(type);
        IsAnimationRunning = false;

        SetAlpha(1f);
    }

    public void BindToCell(Cell cell, float animateTime = 0f)
    {
        cell.BindObject(this, animateTime);
        currentCell = cell;
        SetAnimationRunning(animateTime);
    }

    public void RemoveWithFade()
    {
        AnimateAlpha(
            0f,
            GameManager.Settings.crystalFadeTime,
            () => currentCell.CurrentMap.ClearCell(currentCell)
        );
    }

    public void AnimateAlpha(float value, float animateTime, Action callback = null)
    {
        IsAnimationRunning = true;
        DOTween.Sequence()
            .Append(spriteRenderer.DOFade(value, animateTime))
            .AppendCallback(() => 
            {
                IsAnimationRunning = false;
                callback?.Invoke();
            });
    }

    public void SetAlpha(float value)
    {
        spriteRenderer.DOKill();

        var currColor = spriteRenderer.color;
        spriteRenderer.color = new Color(currColor.r, currColor.g, currColor.b, value);
    }

    private void SetAnimationRunning(float time)
    {
        IsAnimationRunning = true;
        DOTween.Sequence()
            .AppendInterval(time)
            .AppendCallback(() => IsAnimationRunning = false);
    }
}

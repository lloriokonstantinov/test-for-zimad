﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class MatchAnalyzer 
{
    private TileMap currentTargetMap;

    private List<Cell> markedCells;

    public MatchAnalyzer(TileMap target)
    {
        currentTargetMap = target;
        markedCells = new List<Cell>();

        currentTargetMap.OnGridChanged += OnGridChanged;
    }

    private void OnGridChanged()
    {
        var foundedLines = FindMatchLines();

        currentTargetMap.ChangedCells.Clear();
        if (foundedLines.Count > 0)
        {
            currentTargetMap.ProcessMatches(foundedLines);
        }
        else
        {
            currentTargetMap.UndoLastAct();
        }
    }

    private List<List<Cell>> FindMatchLines()
    {
        List<List<Cell>> foundedLines = new List<List<Cell>>();

        foreach (var cell in currentTargetMap.GetCells())
        {
            if (cell == null || cell.IsEmpty)
            {
                continue;
            }

            var verticalLine = new List<Cell>{ cell };
            FillLineFromDirection(cell.Coordinates, Vector2Int.down, verticalLine);
            FillLineFromDirection(cell.Coordinates, Vector2Int.up, verticalLine);

            var horizontalLine = new List<Cell>{ cell };
            FillLineFromDirection(cell.Coordinates, Vector2Int.left, horizontalLine);
            FillLineFromDirection(cell.Coordinates, Vector2Int.right, horizontalLine);

            var bestLine = horizontalLine.Count >= verticalLine.Count ? horizontalLine : verticalLine;

            if (bestLine.Count >= 3)
            {
                foundedLines.Add(bestLine);
                foreach(var lineCell in bestLine)
                {
                    markedCells.Add(lineCell);
                }
            }

        }
        markedCells.Clear();

        return foundedLines;
    }

    private void FillLineFromDirection(Vector2Int startCoords, Vector2Int direction, List<Cell> lineList)
    {
        var type = currentTargetMap.GetCell(startCoords).CurrentCrystal.Type;

        var currentCell = currentTargetMap.GetCell(startCoords + direction);
        while (currentCell != null 
               && !currentCell.IsEmpty 
               && currentCell.CurrentCrystal.Type == type
               && !currentCell.CurrentCrystal.IsAnimationRunning
               && !markedCells.Contains(currentCell))
        {
            lineList.Add(currentCell);
            startCoords += direction;
            currentCell = currentTargetMap.GetCell(startCoords + direction);
        }
    }
}

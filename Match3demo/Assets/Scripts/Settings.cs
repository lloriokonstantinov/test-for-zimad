﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

[CreateAssetMenu(fileName = "game_settings", menuName = "Game Settings")]
public class Settings : ScriptableObject
{
    [System.Serializable]
    public struct CrystalSprite
    {
        public CrystalType crystalType;
        public Sprite sprite;
    }

    public int mainGridSizeX;
    public int mainGridSizeY;
    public float crystalSwipeTime;
    public float crystalFadeTime;
    public float crystalFallTime;
    public CrystalSprite[] crystalSprites;

    public int GetTotalCellsCount()
    {
        return mainGridSizeX * mainGridSizeY;
    }

    public Sprite GetCrystalSprite(CrystalType type)
    {
        return crystalSprites.First((data) => data.crystalType == type).sprite;
    }
}

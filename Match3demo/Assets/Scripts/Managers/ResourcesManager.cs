﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class ResourcesManager 
{
    public static void InitializeResourses()
    {
        var cellPrefab = Resources.Load<Cell>("Prefabs/Cell");
        Pool.Instance.InitObject(cellPrefab, cellPrefab.Name, GameManager.Settings.GetTotalCellsCount());

        var matchObjPrefab = Resources.Load<Crystal>("Prefabs/Crystal");
        Pool.Instance.InitObject(matchObjPrefab, matchObjPrefab.Name, GameManager.Settings.GetTotalCellsCount());
    }
}

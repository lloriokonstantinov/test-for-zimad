﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class Cell : MonoBehaviour, IPoolable
{
    [SerializeField] protected SpriteRenderer spriteRenderer;

    public Crystal CurrentCrystal { get; private set; }
    public Vector2Int Coordinates { get; private set; }
    public TileMap CurrentMap { get; private set; }

    public bool IsEmpty { get { return CurrentCrystal == null; } }

    #region IPoolable
    public string Name => "cell";
    #endregion

    public void Initialize(TileMap parentMap, int x, int y)
    {
        transform.parent = parentMap.transform;
        CurrentMap = parentMap;
        Coordinates = new Vector2Int(x, y);

        var xPos = spriteRenderer.bounds.size.x * (x - parentMap.Width / 2f + 1);
        var yPos = spriteRenderer.bounds.size.y * (y - parentMap.Height / 2f + 1);
        transform.localPosition = new Vector2(xPos, yPos);
    }

    public void BindObject(Crystal newCrystal, float animateTime = 0f)
    {
        RemoveObject();

        newCrystal.transform.SetParent(transform);
        if (animateTime == 0f)
        {
            newCrystal.transform.localPosition = Vector2.zero;
        }
        else
        {
            newCrystal.transform.DOLocalMove(Vector2.zero, animateTime);
        }
        CurrentCrystal = newCrystal;
    }

    public Crystal UnbindObject()
    {
        var obj = CurrentCrystal;
        CurrentCrystal = null;
        return obj;
    }

    public void RemoveObject()
    {
        if (!IsEmpty)
        {
            Pool.Instance.Put(CurrentCrystal);
            CurrentCrystal = null;
        }
    }
}

﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using System.Linq;

public class TileMap : MonoBehaviour
{
    public int Width { get; private set; }
    public int Height { get; private set; }
    public List<Vector2Int> ChangedCells { get; private set; }

    public event Action OnGridChanged = () => { };

    private Cell[,] cells;
    private List<Vector2Int> freeCells;
    private MatchAnalyzer matchAnalyzer;
    private SwitchAction lastSwitchAction;

    private CrystalType[] matchObjectTypes;

    public void Initialize(int width, int height, Vector3 worldPosition)
    {
        Width = width;
        Height = height;
        ChangedCells = new List<Vector2Int>();

        freeCells = new List<Vector2Int>();
        cells = new Cell[width, height];
        matchObjectTypes = System.Enum.GetValues(typeof(CrystalType)) as CrystalType[];

        matchAnalyzer = new MatchAnalyzer(this);

        for (int i = 0; i < width; i++)
        {
            for (int j = 0; j < height; j++)
            {                
                cells[i, j] = Pool.Instance.Take("cell") as Cell;
                cells[i, j].Initialize(this, i, j);
            }
        }
        FillEmptyCells();
        OnGridChanged();
    }

    public Cell GetCell(int x, int y)
    {
        if (x >= 0 && x < Width && y >= 0 && y < Height)
        {
            return cells[x, y];
        }
        return null;
    }

    public Cell GetCell(Vector2Int coords)
    {
        return GetCell(coords.x, coords.y);
    }

    public IEnumerable<Cell> GetCells()
    {
        return cells.Cast<Cell>();
    }

    public void ClearCell(int x, int y)
    {
        var cell = GetCell(x, y);
        if (cell != null)
        {
            ClearCell(cell);
        }
    }

    public void ClearCell(Cell cell)
    {
        cell.RemoveObject();
        freeCells.Add(cell.Coordinates);
    }

    public void ProcessMatches(List<List<Cell>> lines)
    {
        lastSwitchAction = null;
        ClearLines(lines);
        DOTween.Sequence()
            .AppendInterval(GameManager.Settings.crystalFadeTime)
            .AppendCallback(ExecuteGravity)
            .AppendInterval(GameManager.Settings.crystalFallTime)
            .AppendCallback(FillEmptyCells)
            .AppendInterval(GameManager.Settings.crystalFadeTime)
            .AppendCallback(() => OnGridChanged());
    }

    public void ClearLines(List<List<Cell>> lines)
    {
        foreach (var line in lines)
        {
            foreach (var cell in line)
            {
                cell.CurrentCrystal.RemoveWithFade();
            }
        }
    }

    public void FillEmptyCells()
    {
        foreach (var cell in cells.Cast<Cell>().Where((cell) => cell.IsEmpty))
        {
            var matchObj = Pool.Instance.Take("crystal") as Crystal;

            var type = matchObjectTypes[UnityEngine.Random.Range(0, matchObjectTypes.Length)];
            matchObj.Initialize(type, cell);

            ChangedCells.Add(cell.Coordinates);
        }
    }

    public void ExecuteGravity()
    {
        Vector2Int gravityDirection = Vector2Int.down;

        List<Vector2Int> processedCoords = new List<Vector2Int>();
        foreach (var coord in freeCells)
        {
            if (processedCoords.Contains(coord)) continue;
            
            Vector2Int gapCoord = GetBottomCell(coord, gravityDirection);
            if (!GetCell(gapCoord).IsEmpty)
            {
                do
                {
                    gapCoord -= gravityDirection;
                }
                while (!GetCell(gapCoord).IsEmpty);
            }
            ChangedCells.Add(gapCoord);

            Vector2Int cristalCoord = gapCoord - gravityDirection;

            while (GetCell(cristalCoord) != null)
            {
                var crystal = GetCell(cristalCoord).UnbindObject();

                if (crystal != null)
                {
                    crystal.BindToCell(GetCell(gapCoord), GameManager.Settings.crystalFallTime);
                    
                    gapCoord -= gravityDirection;
                }
                processedCoords.Add(cristalCoord);
                ChangedCells.Add(cristalCoord);
                cristalCoord -= gravityDirection;
            }
        }

        freeCells.Clear();
    }

    public Vector2Int GetBottomCell(Vector2Int coord, Vector2Int gravityDirection)
    {
        var bottomCoord = new Vector2Int
        (
            gravityDirection.x == 0 ? coord.x : gravityDirection.x == 1 ? GameManager.Settings.mainGridSizeX - 1 : 0,
            gravityDirection.y == 0 ? coord.y : gravityDirection.y == 1 ? GameManager.Settings.mainGridSizeY - 1 : 0
        );
        return bottomCoord;
    }

    public void TryToSwitch(Vector2Int coord, Vector2Int direction)
    {
        var cell1 = GetCell(coord);
        var cell2 = GetCell(coord + direction);

        if (cell1 == null || cell2 == null || cell1.IsEmpty || cell2.IsEmpty)
        {
            return;
        }

        if (cell1.CurrentCrystal.IsControlUnlocked() && cell2.CurrentCrystal.IsControlUnlocked())
        {
            var switchAct = new SwitchAction(this, cell1, cell2);
            switchAct.Execute();
            lastSwitchAction = switchAct;

            ChangedCells.Add(coord);
            ChangedCells.Add(coord + direction);

            DOTween.Sequence()
                .AppendInterval(GameManager.Settings.crystalSwipeTime)
                .AppendCallback(() =>
                {
                    OnGridChanged();
                });
        }
    }

    public void UndoLastAct()
    {
        if (lastSwitchAction != null)
        {
            lastSwitchAction.Execute();
            lastSwitchAction = null;
        }
    }
}

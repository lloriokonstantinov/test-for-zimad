﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public partial class Crystal : MonoBehaviour
{
    protected const float SWIPE_THRESHOLD = 0.2f;

    protected Vector2 mouseDownPosition;
    protected bool isDragging;

    private void OnMouseDown()
    {
        if (!IsControlUnlocked())
        {
            return;
        }

        mouseDownPosition = Input.mousePosition;
        isDragging = true;
    }

    private void OnMouseDrag()
    {
        if (!IsControlUnlocked() || !isDragging)
        {
            return;
        }

        var mousePos = Input.mousePosition;
        var deltaX = mousePos.x - mouseDownPosition.x;
        var deltaY = mousePos.y - mouseDownPosition.y;

        Vector2 delta = new Vector2(deltaX, deltaY);
        if (delta.magnitude >= SWIPE_THRESHOLD)
        {
            Vector2Int dir;
            if (Mathf.Abs(deltaX) > Mathf.Abs(deltaY))
            {
                dir = Vector2Int.right * (int)Mathf.Sign(deltaX);
            }
            else
            {
                dir = Vector2Int.up * (int)Mathf.Sign(deltaY);
            }

            currentCell.CurrentMap.TryToSwitch(currentCell.Coordinates, dir);
            isDragging = false;
        }
    }

    public bool IsControlUnlocked()
    {
        bool isChanged = currentCell.CurrentMap.ChangedCells.Contains(currentCell.Coordinates);
        return !isChanged && !IsAnimationRunning;
    }
}

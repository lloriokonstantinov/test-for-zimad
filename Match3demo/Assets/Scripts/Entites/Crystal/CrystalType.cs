﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum CrystalType
{
    Blue,
    Green,
    Orange,
    Purple,
    Yellow,
}

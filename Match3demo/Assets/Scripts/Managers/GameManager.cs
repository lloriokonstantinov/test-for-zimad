﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    public static GameManager Instance { get; private set; }
    public static Settings Settings { get; private set; }

    private void Awake()
    {
        if (Instance != null)
        {
            Destroy(this);
        }

        Instance = this;

        DG.Tweening.DOTween.Init().SetCapacity(200, 200); ;

        GameObject pool = new GameObject("Pool");
        pool.AddComponent<Pool>();

        Settings = Resources.Load<Settings>("game_settings");
        ResourcesManager.InitializeResourses();

        GameObject mapGrid = new GameObject("MainGrid");
        var tileMap = mapGrid.AddComponent<TileMap>();
        tileMap.Initialize(Settings.mainGridSizeX, Settings.mainGridSizeY, Vector3.zero);
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SwitchAction
{
    private TileMap tileMap;
    private Cell cell1;
    private Cell cell2;

    public SwitchAction(TileMap tileMap, Cell cell1, Cell cell2)
    {
        this.tileMap = tileMap;
        this.cell1 = cell1;
        this.cell2 = cell2;
    }

    public void Execute()
    {
        var obj1 = cell1.UnbindObject();
        var obj2 = cell2.UnbindObject();
        obj1?.BindToCell(cell2, 0.2f);
        obj2?.BindToCell(cell1, 0.2f);
    }
}

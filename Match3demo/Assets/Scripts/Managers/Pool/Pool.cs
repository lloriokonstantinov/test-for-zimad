﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
/// <summary>
/// Pool data structure
/// </summary>
public class Pool : MonoBehaviour
{
    public static Pool Instance {get; private set;}
    public Transform BasePoolParent { get; private set; }

    private Dictionary<string, MonoBehaviour> knownPrefabs;
    private Dictionary<string, Stack<IPoolable>> pool;

    private void Awake()
    {
        Instance = this;
        BasePoolParent = transform;

        knownPrefabs = new Dictionary<string, MonoBehaviour>();
        pool = new Dictionary<string, Stack<IPoolable>>();

    }

    #region Interface
    public IPoolable Take(string type)
    {
        if (knownPrefabs.ContainsKey(type))
        {
            IPoolable poolObject;
            MonoBehaviour instance;
            if (pool[type].Count != 0)
            {
                poolObject = pool[type].Pop();
                instance = poolObject as MonoBehaviour;
            }
            else
            {
                var prefab = knownPrefabs[type];
                instance = Instantiate(prefab, BasePoolParent);
                poolObject = instance as IPoolable;
            }
            instance.gameObject.SetActive(true);
            return poolObject;
        }
        return null;
    }

    public void InitObject(MonoBehaviour prefab, string key, int count)
    {
        if (!knownPrefabs.ContainsKey(key))
        {
            knownPrefabs.Add(key, prefab);
            pool.Add(key, new Stack<IPoolable>());

            for (int i = 0; i < count; i++)
            {
                var instance = Instantiate(prefab, BasePoolParent);
                instance.transform.SetParent(BasePoolParent);
                instance.gameObject.SetActive(false);
                pool[key].Push(instance as IPoolable);
            }
        }
    }

    public void Put(IPoolable poolObject)
    {
        string type = poolObject.Name;
        if (!knownPrefabs.ContainsKey(type))
        {
            return;
        }
        pool[type].Push(poolObject);
        var instance = poolObject as MonoBehaviour;
        instance.transform.SetParent(BasePoolParent);
        instance.gameObject.SetActive(false);
    }
    #endregion
}

﻿using UnityEngine;

public interface IPoolable
{
    string Name { get; }
}
